//Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data => {
	let list = data.map(todos => {
		return todos.title
	})
	console.log(list)
})


//Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res=> res.json())
.then(list=>console.log(`title: ${list.title} status: ${list.completed}`))


//Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: "POST",
	headers: {'Content-Type':'application/json'},
	body: JSON.stringify({
		userId: 1,
    	title: "do good, die great",
    	completed: false
	})
})
.then(res=>res.json())
.then(newList=> console.log(newList))


//Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PUT',
	headers: {'Content-Type':'application/json'},
	body:JSON.stringify({
		Title: "s28 Activity",
		description: "Postman and REST API Activity",
		Status: "completed",
		dateCompleted: "9/15/21",
		userId:1
	})
})
.then(res=>res.json())
.then(updateList=>console.log(updateList))


//Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {'Content-Type':'application/json'},
	body:JSON.stringify({
		completed: true,
		dateCompleted: "9/15/21"
	})
})
.then(res=>res.json())
.then(updateList=>console.log(updateList))


//Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE'
})
.then(res=>res.json())
.then(data=>console.log(data))
